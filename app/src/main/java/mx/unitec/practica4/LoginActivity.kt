package mx.unitec.practica4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*


const val EXTRA_RFC = "mx.unitec.practica4.RFC"
private lateinit var button: Button

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if(DataPreferences.getStoreRFC(this) != ""){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }



    fun clickRFC(view: View) {

        //Validación del editTextRFC
        button = findViewById(R.id.btnRFC)
        button.setOnClickListener {
            if (txtRFC.text.isEmpty()) {
                txtRFC.error = getString(R.string.error_text)
                return@setOnClickListener
            }
            Toast.makeText(this, R.string.acceso, Toast.LENGTH_LONG).show()
        }

    //Termina validadción


        val intent = Intent(this, MainActivity::class.java).apply {
            putExtra(EXTRA_RFC, txtRFC.text.toString())
        }

        startActivity(intent)
        finish()

    }
}